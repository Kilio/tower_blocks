//
// RNG.hpp for rng in /home/baillel/Documents/coding/2048
// 
// Made by baille_l
// Login   <baillel@epitech.net>
// 
// Started on  Mon Feb 22 22:54:28 2016 baille_l
// Last update Mon Feb 22 23:01:07 2016 baille_l
//

#pragma once

class RNG
{
public:
  static int generate(int max);
  static bool _initialized;
};
