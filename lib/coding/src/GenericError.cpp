//
// GenericError.cpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 23:48:53 2014 bertho_d
// Last update Tue Aug  5 21:50:25 2014 bertho_d
//

#include "GenericError.hpp"

const char	*GenericError::genericErrorMessages[] = {
  "Invalid window dimensions"
  "Invalid image format (please use 24 or 32bbp images)"
};

GenericError::GenericError(t_genericErrCode errcode) : Error(genericErrorMessages[errcode]), _errcode(errcode)
{
}
