/*
** Input.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub/context
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:09:06 2015 Alexis Bertholom
// Last update Tue Nov 21 20:02:43 2017 Lucas
*/

#ifndef INPUT_HPP_
# define INPUT_HPP_

# include <SDL2/SDL_scancode.h>

struct		SDL_MouseMotionEvent;

enum		t_mouseButton
  {
    MOUSEBUTTON_LEFT = 1,
    MOUSEBUTTON_MIDDLE,
    MOUSEBUTTON_RIGHT,
    MOUSEBUTTON_4,
    MOUSEBUTTON_5,
    MOUSEBUTTON_6,
    MOUSEBUTTON_7
  };

class		Input
{
public:
  Input();
  ~Input();

public:
  bool		getKeyState(SDL_Scancode key) const;
  bool		getKeyStateOnce(SDL_Scancode key);
  bool		getMouseButtonState(t_mouseButton button) const;
  const int	*getMousePos() const;
  const int	*getMouseRel() const;
  bool		shouldExit() const;
  void		flushEvents();

private:
  void		setMouse(SDL_MouseMotionEvent &motion);

private:
  bool		_exit;
  bool		_keysState[SDL_NUM_SCANCODES];
  bool		_alreadyPressed[SDL_NUM_SCANCODES];
  int		_mousePos[2];
  int		_mouseRel[2];
  bool		_mouseButtonsState[8];
};

#endif
